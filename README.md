# SurveyDemo #

Surveys@Griffith native shell demo, tracks user's location every hour and stores location records (lat, long & timestamp) of the last 6 hours in memory.

### Overview ###

* Use of two modular classes for MAIN location and CoreData functionality (SurveyLocationModel & CoreDataModel)
* ViewController features "toggle location service" button to start/stop services =For testing=
* Location records will update every hour, keepAlive timer set to 5 minutes but could be extended up to 10 minutes - Tests needed to confirm this.

### Detailed Information ###

* TODO
