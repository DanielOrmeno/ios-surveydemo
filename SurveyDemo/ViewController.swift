//
//  ViewController.swift
//  SurveyDemo
//
//  Created by Daniel Ormeño on 5/12/2014.
//  Copyright (c) 2014 Daniel Ormeño. All rights reserved.
//

import UIKit
import CoreLocation

class ViewController: UIViewController, UIUpdateDelegate {
    
    @IBOutlet weak var permissionLabel: UILabel!
    @IBOutlet weak var locationUpdatesLabel: UILabel!
    @IBOutlet weak var sensibilityLabel: UILabel!
    @IBOutlet weak var recordsLabel: UILabel!
    @IBOutlet weak var lastUpdateLabel: UILabel!
    @IBOutlet weak var serviceButton: UIButton!
    
    //- Records
    @IBOutlet weak var record1: UILabel!
    @IBOutlet weak var record2: UILabel!
    @IBOutlet weak var record3: UILabel!
    @IBOutlet weak var record4: UILabel!
    @IBOutlet weak var record5: UILabel!
    @IBOutlet weak var record6: UILabel!
    
    
    var buttonPressed: Bool = false
    
    // =====================================     INSTANCE VARIABLES / PROPERTIES      =============================//
    var SurveyLocationHelper = SurveyLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        
        //- Set VC as Delegate for UI Updates calls
        self.SurveyLocationHelper.delegate = self
        SurveyLocationHelper.dataManager.delegate = self
        
        //- Updates number of records in memory
        if let fetchResults = self.SurveyLocationHelper.dataManager.getUpdatedRecords() {
            self.didUpdateRecords(fetchResults.count)
        }
        
        if (NSUserDefaults.standardUserDefaults().boolForKey("LocationServices")){
            self.serviceButton.backgroundColor = UIColorFromRGB(0xFF4B00)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func startLocationService(sender: AnyObject) {
        
        if (!buttonPressed) {
            //- Link to cordova plugin
            self.SurveyLocationHelper.updateLocation()
            
            if (self.SurveyLocationHelper.updatesEnabled) {
                self.buttonPressed = true
                self.serviceButton.backgroundColor = UIColorFromRGB(0xFF4B00)
            }
            
        } else {
            self.SurveyLocationHelper.stopLocationServices()
            buttonPressed = false
            println("services stopped")
            self.serviceButton.backgroundColor = UIColor.blackColor()
            self.didUpdateLocation("Not available")
        }
    }
    
    func UIColorFromRGB(rgbValue: UInt) -> UIColor {
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    // =====================================     DELEGATE METHODS      =============================//
    func didUpdatePermissions (permission: String){
        self.permissionLabel.text = permission
    }
    
    func didUpdateLocation (location: String){
        self.locationUpdatesLabel.text = location
    }
    
    
    func didUpdateSensibility (sensibility: String){
        self.sensibilityLabel.text = sensibility
    }
    
    func didUpdateRecords (records: Int){
        self.recordsLabel.text = String(records)
    }
    
    func didUpdateLastLocation (lastUpdate: String){
        self.lastUpdateLabel.text = lastUpdate
    }
    
    func didUpdateAllRecords(newLabels: [String]){
        
        switch newLabels.count {
        case 1:
            self.record1.text = newLabels[0]
        case 2:
            self.record1.text = newLabels[0]
            self.record2.text = newLabels[1]
        case 3:
            self.record1.text = newLabels[0]
            self.record2.text = newLabels[1]
            self.record3.text = newLabels[2]
        case 4:
            self.record1.text = newLabels[0]
            self.record2.text = newLabels[1]
            self.record3.text = newLabels[2]
            self.record4.text = newLabels[3]
        case 5:
            self.record1.text = newLabels[0]
            self.record2.text = newLabels[1]
            self.record3.text = newLabels[2]
            self.record4.text = newLabels[3]
            self.record5.text = newLabels[4]
        case 6:
            self.record1.text = newLabels[0]
            self.record2.text = newLabels[1]
            self.record3.text = newLabels[2]
            self.record4.text = newLabels[3]
            self.record5.text = newLabels[4]
            self.record6.text = newLabels[5]
        default:
            println("error in ui parser")
        }
    }
    
    func alertPermissions() {
        let alert = UIAlertController(title: "Location Services Disabled", message: "Please enable \"Always\" location services in settings", preferredStyle: UIAlertControllerStyle.Alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: { action in
            switch action.style {
            case .Default:
                println("accepted")
            case .Cancel:
                println("cancel")
            case .Destructive:
                println("destructive")
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil))
        
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
}

