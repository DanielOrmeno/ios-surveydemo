//
//  UIUpdateProtocol.swift
//  SurveyDemo
//
//  Created by Daniel Ormeño on 21/12/2014.
//  Copyright (c) 2014 Daniel Ormeño. All rights reserved.
//

import Foundation

protocol UIUpdateDelegate {
    
    func didUpdatePermissions (permission: String)
    
    func didUpdateLocation (location: String)
    
    func didUpdateSensibility (sensibility: String)
    
    func didUpdateRecords (records: Int)
    
    func didUpdateLastLocation (lastUpdate: String)

    func didUpdateAllRecords ([String])
    
    func alertPermissions ()
    
}