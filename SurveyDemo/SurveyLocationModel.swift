/*******************************************************************************************************************
|   File: LocationModel.swift
|   Proyect: Surveys@Griffith
|
|   Description: - Swift class to be integrated onto Surveys@Griffith iOS Application
|
|   Copyright (c) 2014 Daniel Ormeño. All rights reserved.
*******************************************************************************************************************/

import CoreLocation
import CoreData
import UIKit

class SurveyLocationManager : NSObject, CLLocationManagerDelegate {
    
    // =====================================     INSTANCE VARIABLES / PROPERTIES      =============================//
    
    //- Date Format
    let DATEFORMAT : String = "M/d/yy, h:mm:ss a"
    
    //- Time interval for scan - Default: 60 minutes
    let KeepAliveTimeInterval : Double = 5*60
    
    //- NSTimer object for scheduling accuracy changes
    var timer = NSTimer()
    
    //- Controls button calls
    var updatesEnabled = false
    
    //- Location Manager - CoreLocation Framework
    let locationManager = CLLocationManager()
    
    //- DataManager Object - Manages data in memory based on the CoreData framework
    let dataManager = DataManager()
    
    //- UIUpdateDelegate object for ui updates
    var delegate: UIUpdateDelegate?
    
    //- UIBackgroundTask
    var bgTask = UIBackgroundTaskInvalid
    
    //- NSNotificationCenter to handle changes in app state
    let defaultCenter: NSNotificationCenter = NSNotificationCenter.defaultCenter()
    
    //- NSUserDefaults - LocationServicesControl_KEY to be set to TRUE when user has enabled location services.
    let UserDefaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
    let LocationServicesControl_KEY: String = "LocationServices"
    
    // =====================================     CLASS CONSTRUCTORS      =========================================//
    
    override init () {
        
        //- Super Class Constructor
        super.init()
        
        // Location Manager configuration --------------------------------------------------------------------------
        self.locationManager.delegate = self
        
        //- Authorization for utilization of location services for background process
        if (CLLocationManager.authorizationStatus() != CLAuthorizationStatus.Authorized) {
            self.locationManager.requestAlwaysAuthorization()
        }
        // END: Location Manager configuration ---------------------------------------------------------------------
        
        //- NSNotificationCenter configuration for handling transitions in the App's Lifecycle
        let terminateSelector: Selector = "appWillTerminate:"
        let relaunchSelector: Selector = "appIsRelaunched:"
        self.defaultCenter.addObserver(self, selector: terminateSelector, name: UIApplicationWillTerminateNotification, object: nil)
        self.defaultCenter.addObserver(self, selector: relaunchSelector, name: UIApplicationDidFinishLaunchingNotification, object: nil)
        
        //- Save Location Services DISABLED to NSUserDefaults
        //self.UserDefaults.setBool(false, forKey: self.LocationServicesControl_KEY)
        
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // =====================================     CLASS METHODS      ===============================================//
    
    func appWillTerminate (notification: NSNotification){
        
        let ServicesEnabled = self.UserDefaults.boolForKey(self.LocationServicesControl_KEY)
        
        //- Stops Standard Location Services if they have been enabled by the user
        if ServicesEnabled {
            
            //- Stop Location Updates
            self.locationManager.stopUpdatingLocation()
            
            //- Stops Timer
            self.timer.invalidate()
            
            //- Enables Significant Location Changes services to restart the app ASAP
            self.locationManager.startMonitoringSignificantLocationChanges()
        }
        
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    func appIsRelaunched (notification: NSNotification) {
        
        //- Stops Significant Location Changes services when app is relaunched
        self.locationManager.stopMonitoringSignificantLocationChanges()
        
        let ServicesEnabled = self.UserDefaults.boolForKey(self.LocationServicesControl_KEY)
        
        //- Re-Starts Standard Location Services if they have been enabled by the user
        if (ServicesEnabled) {
            self.updateLocation()
        }
    }
    
    func updateLocation () {
        
        if (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.Authorized){
            
            //- Location Accuracy, properties & Distance filter
            self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
            self.locationManager.distanceFilter = kCLDistanceFilterNone
            
            //- Start receiving location updates
            self.locationManager.startUpdatingLocation()
            
            self.updatesEnabled = true;
            
            //- Updates labels in view controller.
            self.changeAccuracyUIValues()
            
            //- Save Location Services ENABLED to NSUserDefaults
            self.UserDefaults.setBool(true, forKey: self.LocationServicesControl_KEY)
            
        } else {
            
            //- Unauthorized, requests permissions again and makes recursive call
            self.delegate?.didUpdatePermissions("Unauthorized")
            self.delegate?.didUpdateLocation("Disabled")
            self.delegate?.alertPermissions()
        }
        
    }
    
    func stopLocationServices() {
        
        self.updatesEnabled = false;
        
        //- Stop Location Updates
        self.locationManager.stopUpdatingLocation()
        
        //- Stops Timer
        self.timer.invalidate()
        
        //- Save Location Services DISABLED to NSUserDefaults
        self.UserDefaults.setBool(false, forKey: self.LocationServicesControl_KEY)
    }
    
    func changeLocationAccuracy (){
        
        let CurrentAccuracy = self.locationManager.desiredAccuracy
        
        switch CurrentAccuracy {
            
        case kCLLocationAccuracyBest: //- Decreses Accuracy
            
            self.locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers
            self.locationManager.distanceFilter = 99999
            
        case kCLLocationAccuracyThreeKilometers: //- Increaces Accuracy
            
            self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
            self.locationManager.distanceFilter = kCLDistanceFilterNone
            
        default:
            println("Accuracy not Changed")
        }
        
        //- Update UI Values - TODO: REMOVE UPON IMPLEMENTING IN CORDOVA APP
        self.changeAccuracyUIValues()
    }
    
    func isUpdateValid (newDate: NSDate) -> Bool{
        
        let SixtyMINUTES : NSTimeInterval = 60*60
        
        var dateString : String = "" //- Null String
        var interval = NSTimeInterval()
        
        if let newestRecord = self.dataManager.getLastRecord() {
            dateString = newestRecord.timestamp
        }
        
        //- Convert String from CoreData to NSDate Object
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = DATEFORMAT
        let date : NSDate? = dateFormatter.dateFromString(dateString)
        
        if let x = date {
            interval = newDate.timeIntervalSinceDate(x)
        }
        
        if ((interval==0)||(interval>=SixtyMINUTES)){
            println("Location is VALID with interval:\(interval)")
            return true
        } else {
            return false
        }
    }
    
    // =====================================     CLLocationManager Delegate Methods    ===========================//
    
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [CLLocation]!) {
        
        self.bgTask = UIApplication.sharedApplication().beginBackgroundTaskWithExpirationHandler({
            UIApplication.sharedApplication().endBackgroundTask(self.bgTask)
            self.bgTask = UIBackgroundTaskInvalid
        })
        
        //- parse last known location
        let newLocation = locations.last!
        
        // Filters bad location updates cached by the OS -----------------------------------------------------------
        let Interval: NSTimeInterval = newLocation.timestamp.timeIntervalSinceNow
        
        let accuracy = self.locationManager.desiredAccuracy
        
        if ((abs(Interval)<5)&&(accuracy != kCLLocationAccuracyThreeKilometers)) {
            
            //- Updates Persistent records through the DataManager object
            if isUpdateValid(newLocation.timestamp) {
                dataManager.updateLocationRecords(newLocation)
            }
            
            /* Timer initialized everytime an update is received. When timer expires, reverts accuracy to HIGH, thus
            enabling the delegate to receive new location updates */
            self.timer = NSTimer.scheduledTimerWithTimeInterval(self.KeepAliveTimeInterval, target: self, selector: Selector("changeLocationAccuracy"), userInfo: nil, repeats: false)
            //- Lowers accuracy to avoid battery drainage
            self.changeLocationAccuracy()
            
        } else {
            //println("Dismissed location record")
        }
        // END: Filters bad location updates cached by the OS ------------------------------------------------------
    }
    
    func locationManager(manager: CLLocationManager!, didFailWithError error: NSError!) {
        println("Location update error: \(error.localizedDescription)")
    }
    
    
    // =====================================     REMOVE BELOW WHEN IMPLEMENTING CORDOVA    ===========================//
    
    func changeAccuracyUIValues() {
        
        self.delegate?.didUpdatePermissions("Always enabled")
        
        if (self.updatesEnabled){
        self.delegate?.didUpdateLocation("Every \(KeepAliveTimeInterval/60) mins")
        }
        
        let accuracy = self.locationManager.desiredAccuracy.description
        let filter = self.locationManager.distanceFilter.description
        
        self.delegate?.didUpdateSensibility("\(accuracy)/Filter: \(filter) m")
    }
    
    
    
} // END OF SurveyLocationManger CLASS