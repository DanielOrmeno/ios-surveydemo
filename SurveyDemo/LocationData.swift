//
//  LocationData.swift
//  SurveyDemo
//
//  Created by Daniel Ormeño on 5/12/2014.
//  Copyright (c) 2014 Daniel Ormeño. All rights reserved.
//

import Foundation
import CoreData

class LocationData: NSManagedObject {

    @NSManaged var lat: NSNumber
    @NSManaged var long: NSNumber
    @NSManaged var alt: NSNumber
    @NSManaged var timestamp: String
    
    class func createInManagedObjectContext(moc: NSManagedObjectContext, lat: Double, long: Double, alt: Double, timestamp: String) -> LocationData {
        let newItem = NSEntityDescription.insertNewObjectForEntityForName("LocationData", inManagedObjectContext: moc) as LocationData
        newItem.lat = lat
        newItem.long = long
        newItem.alt = alt
        newItem.timestamp = timestamp
        
        return newItem
    }

}
